function NewCalc() {
    let calc = document.getElementById("calc");
    let checkUp = document.getElementById("checkUp");
    let autos = document.getElementById("autos");
    let radios = document.getElementsByName("options");
    let result = document.getElementById("Result");
    let checkDiv = document.getElementById("checkboxes");
    let radioDiv = document.getElementById("radios");

    if (autos.selectedIndex == 2) {
        radioDiv.style.display = "block";
        checkDiv.style.display = "none";
    } else if (autos.selectedIndex == 3) {
        radioDiv.style.display = "none";
        checkDiv.style.display = "block";
        uncheckAllRadio(radios);
    } else {
        radioDiv.style.display = "none";
        checkDiv.style.display = "none";
        uncheckAllRadio(radios);
    }
    let price = 0;
    price += parseInt(autos.options[autos.selectedIndex].value);
    for (let radio of radios) {
        if (radio.checked) {
            price += parseInt(radio.value);
        }
    }
    price += (checkUp.checked == true) ? parseInt(checkUp.value) : 0;
    price = parseInt(calc.value) * price;

    if (Number.isNaN(price)) {
        result.placeholder = "Enter correct values please!";
    } else {
        result.placeholder = price;
    }
}

function uncheckAllRadio(radios) {
    for (let radio of radios) {
        radio.checked = false;
    }
}