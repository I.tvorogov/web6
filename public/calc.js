function calculate() {
    let price = Number(document.getElementById("price").value);
    let number = Number(document.getElementById("number").value);
    let result = price * number;
    if (Number.isNaN(result)) {
        document.getElementById("result").innerHTML = "Error!";
    } else {
        document.getElementById("result").innerHTML = result;
    }
    document.getElementById("price").value = "";
    document.getElementById("number").value = "";
}